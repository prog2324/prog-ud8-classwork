package act06;

import java.util.HashSet;
import java.util.Scanner;

public class act06 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HashSet<String> nombres = new HashSet<>();

        System.out.println("Introdueix noms de persones separats per espais");
        while (true) {
            String nombre = scanner.next();
            if (nombre.equalsIgnoreCase("FIN")) {
                break;
            }
            System.out.printf("Insertando %s (%d)\n", nombre, nombre.hashCode());
            nombres.add(nombre);            
        }

        // Bucle foreach
        for (String nombre : nombres) {
            System.out.printf("  - %s (%d)\n", nombre, nombre.hashCode());
        }

        // Iterator: ver en las diapositivas.
        // ...
    }
}
