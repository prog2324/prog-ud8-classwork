package act05parelles.tiposproducciones;

import act05parelles.Data;
import act05parelles.Formato;

/**
 *
 * @author sergio
 */
public class Serie extends Produccion {

    private int temporada;
    private int capitulos;

    private static final float PRECIO = 24f;

    public Serie(String titulo, Formato formato, int duracion, Data fechaLanzamiento, int temporada, int capitulos) {
        super(titulo, formato, duracion, fechaLanzamiento, PRECIO);
        this.temporada = temporada;
        this.capitulos = capitulos;
    }

    public Serie(String titulo, Formato formato, Data fechaLanzamiento, int temporada, int capitulos) {
        super(titulo, formato, 2400, fechaLanzamiento, PRECIO);
        this.temporada = temporada;
        this.capitulos = capitulos;
    }

    @Override
    public void mostrarDetalle() {
        System.out.println("-------------------Serie--------------------");
        super.mostrarDetalle();
        System.out.printf("Temporada %d - Capítulos: %d %n", this.temporada, this.capitulos);
        System.out.println("Duración: " + super.getDuracionHorasMinutosSegundos());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (Serie) temporada = " + this.temporada + ", capítulos = " + this.capitulos;
    }

}
