package act05parelles.tiposproducciones;

import act05parelles.Data;
import act05parelles.Formato;

/**
 *
 * @author sergio
 */
public class Documental extends Produccion {

    private String investigador;
    private String tema;
    
   private static final float PRECIO = 15f;

    
    public Documental(String titulo, Formato formato, int duracion, Data fechaLanzamiento, String investigador, String tema) {
        super(titulo, formato, duracion, fechaLanzamiento, PRECIO);
        this.investigador = investigador;
        this.tema = tema;
    }
    
    public Documental(String titulo, Formato formato, Data fechaLanzamiento, String investigador, String tema) {
        super(titulo, formato, 1800, fechaLanzamiento, PRECIO);
        this.investigador = investigador;
        this.tema = tema;
    }

    @Override
    public void mostrarDetalle() {
        System.out.println("---------------- Documental ---------------");
        super.mostrarDetalle();
        System.out.println("Investigador: " + investigador);
        System.out.println("Duración: " + super.getDuracionHorasMinutosSegundos());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (Documental)" + " investigador= " + this.investigador + ", tema= " + this.tema;
    }

}
