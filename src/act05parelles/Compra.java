package act05parelles;

import act05parelles.tiposproducciones.Produccion;
import java.util.Objects;


public class Compra {
    private Cliente cliente;        // podría ser interesante guardar también este dato.
    private Produccion produccion;
    private Data fechaRealizado;
    private boolean esFinalizado;
    private String comentario;

    public Compra(Cliente cliente, Produccion produccion, Data fechaRealizado, boolean esFinalizado, String comentario) {
        this.cliente = cliente;
        cliente.anyadirCompra(this);
        this.produccion = produccion;
        this.fechaRealizado = fechaRealizado;
        this.esFinalizado = esFinalizado;
        this.comentario = comentario;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Compra other = (Compra) obj;
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        return Objects.equals(this.produccion, other.produccion);
    }



    public float getPrecio() {
        return produccion.getPrecio();
    }

    @Override
    public String toString() {
        return String.format("%s - fecha: %s - %s", produccion.getTitulo(), fechaRealizado, comentario);
    }
}
