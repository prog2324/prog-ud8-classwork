package act05parelles;

import act05parelles.tiposproducciones.*;

/**
 *
 * @author sergio
 */
public class BatoiFlix {

    public static void main(String[] args) {
        Data fecha = new Data(19, 2, 2021);
        Documental dreamSongs = new Documental("Dream Songs", Formato.MPG, 2400, fecha, "Enrique Juncosa", "Movimiento Hippie");
        dreamSongs.setDescripcion("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        fecha = new Data(20, 6, 2020);
        Pelicula newsOfTheWorld = new Pelicula("News of the World", Formato.AVI, 7200, fecha, "Tom Hanks", "Carolina Betller");
        newsOfTheWorld.setDescripcion("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        fecha = new Data(04, 06, 2001);
        Serie elHacker = new Serie("El hacker", Formato.FLV, 3600, fecha, 1, 20);
        elHacker.setDescripcion("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        System.out.println("\nListado Producciones (Uso toString()) \n");
        System.out.println(newsOfTheWorld);
        System.out.println(dreamSongs);
        System.out.println(elHacker);

        System.out.println("\nDetalle Producciones (Uso mostrarDetalle())\n");
        newsOfTheWorld.mostrarDetalle();
        dreamSongs.mostrarDetalle();
        elHacker.mostrarDetalle();

        Cliente cliente1 = new Cliente(new Data(1, 05, 2001), "cliente1@a.com", "Pepe", "Pérez", "1111111A");
        Cliente cliente2 = new Cliente(new Data(2, 01, 1994), "cliente2@a.com", "Pepa", "Pig", "2222222A");
        Cliente cliente3 = new Cliente(new Data(10, 1, 1956), "cliente3@a.com", "Pepo", "Ron", "3333333A");        
        
        Compra compra1 = new Compra(cliente1, dreamSongs, new Data(12, 12, 2022), false, "Genial");
        Compra compra2 = new Compra(cliente2, newsOfTheWorld, new Data(9, 4, 2020), false, "Normalita");
        Compra compra3 = new Compra(cliente3, elHacker, new Data(3, 10, 2020), false, "Estupendo");
        Compra compra4 = new Compra(cliente2, dreamSongs, new Data(12, 1, 2022), false, "Fatal");
        
        System.out.println(cliente1);
        System.out.println(cliente2);
        System.out.println(cliente3);
    }
}
