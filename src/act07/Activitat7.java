package act07;

import java.util.HashSet;
import java.util.Set;

public class Activitat7 {

    public static void main(String[] args) {
        Set<Participante> prueba1 = new HashSet<>();
        prueba1.add(new Participante("Toni García", "64112243L", 10.12f));
        prueba1.add(new Participante("Elena Compte", "72363370B", 12.23f));
        prueba1.add(new Participante("Maria Pérez", "56099532W", 15.23f));
        prueba1.add(new Participante("Juan Magan", "89367935D", 18.15f));
        System.out.println("==== Participants Prova 1 ====​");
        mostrarLista(prueba1);
        System.out.println("Total participantes prueba1:" + prueba1.size());

        Set<Participante> prueba2 = new HashSet<>();
        prueba2.add(new Participante("Toni García", "64112243L", 19.32f));
        prueba2.add(new Participante("Ernesto Compte", "39059576X", 20.5f));
        prueba2.add(new Participante("Mari Carmen Ruiz", "26723726A", 21.5f));
        prueba2.add(new Participante("Elena García", "87787367R", 20.05f));
        System.out.println("==== Participants Prova 2 ====​");
        mostrarLista(prueba2);
        System.out.println("Total participantes prueba2:" + prueba2.size());

        Set<Participante> unionPruebas = new HashSet<>();
        unionPruebas.addAll(prueba1);
        unionPruebas.addAll(prueba2);
        System.out.println("==== Participants únics ====​");
        mostrarLista(unionPruebas);
        System.out.println("Total participantes: " + unionPruebas.size());

    }

    private static void mostrarLista(Set<Participante> lista) {
        for (Participante p : lista) {
            System.out.println(" - " + p + "(" + p.hashCode() + ")");
        }
    }

}
