package act07;

import java.util.Objects;

public class Participante {

    private String nombre;
    private String dni;
    private float tiempo;

    public Participante(String nombre, String dni, float tiempo) {
        this.nombre = nombre;
        this.dni = dni;
        this.tiempo = tiempo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.dni);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participante other = (Participante) obj;
        return Objects.equals(this.dni, other.dni);
    }

    @Override
    public String toString() {
        return nombre + ", dni=" + dni + ", tiempo=" + tiempo + "seg.";
    }

}
