package act02;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author sergio
 */
public class Comanyeros {
    public static void main(String[] args) {
        Alumno companyero1 = new Alumno("Roberto", "Hidalgo");
        Alumno companyero2 = new Alumno("Raul", "Valor");
        Alumno companyero3 = new Alumno("Sergio", "Galisteo");
        Alumno companyero4 = new Alumno("Empar", "Carrasquer");
        Alumno companyero5 = new Alumno("Cristina", "Balaguer");
        
        ArrayList<Alumno> companyeros = new ArrayList<>();
        companyeros.add(companyero1);
        companyeros.add(companyero2);
        companyeros.add(companyero3);
        companyeros.add(companyero4);
        companyeros.add(companyero5);
        companyeros.add(new Alumno("Arturo", "Candela"));
        
        Iterator i = companyeros.iterator();
        while(i.hasNext()) {
            Alumno a = (Alumno) i.next();
            System.out.println(a);
        }
        
        //--------------------------------
        Alumno c1 = new Alumno("Sergio", "Galisteo"); 
        if (companyeros.contains(c1)) {
            System.out.println("Ya existe " + c1);
        }
        else {
            System.out.println("No existe " + c1);
        }     
        
        Alumno c2 = new Alumno("Carlos", "Huelmo"); 
        if (companyeros.contains(c2)) {
            System.out.println("Ya existe " + c2);
        }
        else {
            System.out.println("No existe " + c2);
        }        
        
    }
 
}
