/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package act04;

import java.util.Objects;

/**
 *
 * @author sergio
 */
public class Videojoc {
    private String titol;
    private String genere;
    private float preu;
    private boolean multijugador;

    public Videojoc(String titol, String genere, float preu, boolean multijugador) {
        this.titol = titol;
        this.genere = genere;
        this.preu = preu;
        this.multijugador = multijugador;
    }
    
    public Videojoc(String titol) {
        this.titol = titol;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Videojoc other = (Videojoc) obj;
        return Objects.equals(this.titol.toLowerCase(), other.titol.toLowerCase());
    }

    @Override
    public String toString() {
        return "[" + this.titol +"]";
    }
    
    


    
    
}
