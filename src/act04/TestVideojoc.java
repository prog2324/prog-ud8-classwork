/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package act04;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class TestVideojoc {
    public static void main(String[] args) {
        List<Videojoc> llistat = new ArrayList<>();
        llistat.add(new Videojoc("Fortnite", "Acción", 40, true));
        llistat.add(new Videojoc("Fifa", "Deportes", 50, true));
        llistat.add(new Videojoc("Gran Theft Auto", "Acción", 80, true));
        llistat.add(new Videojoc("Minecraft", "Simulación", 60, true));
        llistat.add(new Videojoc("AnimalCrossing", "Simulación", 30, true));
        
        System.out.println("Videojocs disponibles");
        for (Videojoc v: llistat) {
            System.out.println("- " + v);
        }
        System.out.println("----------------");
        
        Scanner teclat = new Scanner(System.in);
        System.out.print("Introdueix videojoc que vols llogar: ");
        String nomVideojoc = teclat.next();
        Videojoc videojocUsuari = new Videojoc(nomVideojoc);
        
        if (llistat.contains(videojocUsuari)) {
            System.out.println(videojocUsuari + " està disponible");
        }
        else {
            System.out.println(videojocUsuari + " no està disponible");
        }
        
        
    }
}
