package act08parelles;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ForoInteligente extends Foro {

    private Set<String> palabrasProhibidas;
    private Set<Mensaje> listaSpam;

    public ForoInteligente(String nombre, String... palabrasProhibidas) {
        super(nombre);
        this.listaSpam = new HashSet<>();
        this.palabrasProhibidas = new HashSet<>(List.of(palabrasProhibidas));
    }

    @Override
    public boolean registrarEntrada(String descripcion) {
        for (String palabraProhibida : palabrasProhibidas) {
            if (descripcion.contains(palabraProhibida)) {
                this.listaSpam.add(new Mensaje(descripcion));
                return false;
            }
        }
        return super.registrarEntrada(descripcion);
    }

    @Override
    public String toString() {
        return String.format("%sSucesos spam: %d\n", super.toString(), listaSpam.size());
    }

    @Override
    protected String getTipo() {
        return "Inteligente";
    }
}
