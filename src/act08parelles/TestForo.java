package act08parelles;

import java.util.LinkedList;

public class TestForo {

    public static void main(String[] args) {

        LinkedList<Foro> foros = new LinkedList<>();    // Se usa linkedlist aunque, para este caso, no aparta nada especialmente.

        Foro basica1 = new Foro("Foro normal");
        foros.add(basica1);

        ForoInteligente inteligente1 = new ForoInteligente("Foro inteligente sin palabras prohibidas");
        foros.add(inteligente1);

        ForoInteligente inteligente2 = new ForoInteligente("Foro inteligente con palabras prohibidas", "foto", "descargar");
        foros.add(inteligente2);

        for (Foro foro: foros) {
            foro.registrarEntrada("¿Habéis utilizado el servicio de Instagram, ayer me saltó un error en el servicio?");
            foro.registrarEntrada("Los de thepiratebay informaron que podría haber sido un ataque");
            foro.registrarEntrada("Algunos usuarios compartieron la publicación en facebook junto con una foto en la que aparecían los causantes del problema. ¡Era un error de programación!");
        }

        for (Foro foro: foros) {
            System.out.println(foro);
        }
    }
}
