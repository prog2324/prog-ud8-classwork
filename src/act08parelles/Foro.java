package act08parelles;

import java.util.ArrayList;
import java.util.List;

public class Foro {

    private String nombre;
    private List<Mensaje> entradas;

    public Foro(String nombre) {
        this.entradas = new ArrayList<>();
        this.nombre = nombre;
    }

    public boolean registrarEntrada(String descripcion) {
        return this.entradas.add(new Mensaje(descripcion));
    }

    @Override
    public String toString() {
        StringBuilder entradaBuilder = new StringBuilder();
        for (Mensaje entrada : entradas) {
            entradaBuilder.append("\n" + entrada);
        }
        return String.format("----- Foro %s(%d mensajes) %s ------ %s \n",
                getTipo(),
                entradas.size(), nombre, entradaBuilder);
    }

    protected String getTipo() {
        return "";
    }
}
