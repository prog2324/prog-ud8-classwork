package act01;

import java.util.ArrayList;

/**
 *
 * @author sergio
 */
public class Arcoiris {

    public static void main(String[] args) {
        ArrayList<String> arcoiris = new ArrayList<>();

        arcoiris.add("rojo");
        arcoiris.add("naranja");
        arcoiris.add("amarillo");
        arcoiris.add("verde");
        arcoiris.add("cian");
        arcoiris.add("azul");
        arcoiris.add("lila");

        System.out.println("Cantidad elementos: " + arcoiris.size());
        int pos = arcoiris.indexOf("rojo");
        System.out.println("Posición del rojo: " + pos);
        
        System.out.println("--- Usando for ---");
        for (int i = 0; i < arcoiris.size(); i++) {
            System.out.println(arcoiris.get(i));
        }
        
        System.out.println("--- Usando foreach ---");
        for(String c: arcoiris) {
            System.out.println(c);
        }
        
        String colorUsuario1 = "negro";        
        if (arcoiris.contains(colorUsuario1)) {
            System.out.println("Ya existe el " + colorUsuario1);
        }
        else {
            arcoiris.add(colorUsuario1);
            System.out.println("No existe el " + colorUsuario1 + ". Lo añadimos.");
        }     
        
        String colorUsuario2 = "naranja";        
        if (arcoiris.contains(colorUsuario2)) {
            System.out.println("Ya existe el " + colorUsuario2);
        }
        else {
            arcoiris.add(colorUsuario2);
            System.out.println("No existe el " + colorUsuario2 + ". Lo añadimos.");
        }     
    }
}
