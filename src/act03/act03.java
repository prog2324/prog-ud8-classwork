package act03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author sergio
 */
public class act03 {

    public static void main(String[] args) {
        List<Integer> valores = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            int v = (int) Math.round(Math.random() * 100);
            valores.add(v);
        }

        // Lista de valores
        for (Integer v : valores) {
            System.out.print(v + " ");
        }
        System.out.println("\n-------");

        // Cálculo de la suma
        int suma = suma(valores);
        System.out.println("Suma: " + suma);

        // Cálculo de la media
        double media = suma / valores.size();
        System.out.println("Media: " + media);

        // Cálculo del mayor
        int mayor = maximo(valores);
        System.out.println("Max: " + mayor);

        // Cálculo del menor
        int menor = minimo(valores);
        System.out.println("Min: " + menor);

    }

    private static int suma(List<Integer> valores) {
        int suma = 0;
        for (Integer v : valores) {
            suma += v;
        }
        return suma;
    }

    private static int maximo(List<Integer> valores) {
        int mayor = 0;
        for (Integer v : valores) {
            if (v > mayor) {
                mayor = v;
            }
        }
        // mayor = Collections.max(valores);
        return mayor;
    }

    private static int minimo(List<Integer> valores) {
        int menor = Integer.MAX_VALUE;
        for (Integer v : valores) {
            if (v < menor) {
                menor = v;
            }
        }
        // menor = Collections.min(valores);
        return menor;
    }

}
